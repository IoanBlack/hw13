<?php
$searchWords = ['php','html','интернет','Web'];
$searchStrings = [
    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом',
    'PHP - это распространенный язык программирования с открытым исходным кодом.',
    'PHP сконструирован специально для ведения Web-разработок и его код может внедряться непосредственно в HTML'
];

function findWords($words,$strings)
{
    $regEx = '/(' . implode('|', $words) . ')/ui';

    foreach ($strings as $num => $string) {
        preg_match_all($regEx, $string, $matches,PREG_PATTERN_ORDER);

        echo 'В предложении # ' . ++$num .  ' есть слова: ' . implode(',', $matches[0]), '<br>';
    }
}

findWords($searchWords,$searchStrings);